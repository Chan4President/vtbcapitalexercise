﻿namespace StringEvaluation.Console
{
    interface IStringEvaluator
    {
        double Evaluate(string input);
    }
}
