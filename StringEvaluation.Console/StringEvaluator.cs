﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace StringEvaluation.Console
{
    /// <summary>
    /// Assumptions: 
    /// 1) All operands will either be an integer or a double
    /// 2) String input is a valid mathematical operation
    /// 3) Numbers are never followed by an opening bracket eg  "2("
    /// </summary>
    public class StringEvaluator : IStringEvaluator
    {
        private Regex _componentLocatorRegex;
        private IList<char> _operandElements = new List<char>()
        {
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '.',
        };

        private IList<char> _operatorsList = new List<char>()
        {
            '+',
            '-',
            '/',
            '*',
        };
                
        public StringEvaluator()
        {
            _componentLocatorRegex = new Regex(@"(?:\((?:(\d*(?:\.\d*)*)\s*([+-\/*])\s*(\d*(?:\.\d*)*)\)))");
            
        }

        public double Evaluate(string inputQuery)
        {
            var flattenedQuery = EvaluateNestedComponents(inputQuery);

            var overallResult = ComputingFlattenedQuery(flattenedQuery);
            return overallResult;
        }

        private string EvaluateNestedComponents(string nestedComponent)
        {
            string flattenedQuery = nestedComponent;
            var matchCollection = _componentLocatorRegex.Matches(nestedComponent);

            foreach (Match match in matchCollection)
            {
                var componentResult = EvaluateQueryComponent(match);
                if (componentResult.HasValue)
                    flattenedQuery = flattenedQuery.Replace(match.Value, componentResult.Value.ToString());
                else
                    throw new ArgumentException($"Bad mathematical expression: {match.Value}");
            }

            if (flattenedQuery.Contains('('))
                EvaluateNestedComponents(flattenedQuery);

            return flattenedQuery;
        }

        private double? EvaluateQueryComponent(Match componentMatch)
        {
            var operand1 = Double.Parse(componentMatch.Groups[1].Value);
            var operand2 = Double.Parse(componentMatch.Groups[3].Value);
            var operatorString = componentMatch.Groups[2].Value;

            return ApplyOperator(operand1, operand2, operatorString); 
        }

        private double ComputingFlattenedQuery(string flattenedQuery)
        {
            var listOfOperands = new List<string>();
            var listOfOperators = new List<string>();
            var flattenedQueryArray = flattenedQuery.Replace(" ", string.Empty).ToCharArray();

            string tempOperand = string.Empty;

            for (var i = 0; i < flattenedQueryArray.Length; i++)
            {
                if (_operandElements.Contains(flattenedQueryArray[i]))
                {
                    tempOperand += flattenedQueryArray[i];
                }

                if (_operatorsList.Contains(flattenedQueryArray[i]))
                {
                    listOfOperands.Add(tempOperand);
                    tempOperand = string.Empty;
                    listOfOperators.Add(flattenedQueryArray[i].ToString());
                }

                if (i == flattenedQueryArray.Length - 1)
                    listOfOperands.Add(tempOperand);
            }

            double accumulator = Double.Parse(listOfOperands[0]);

            for (var i = 0; i < listOfOperators.Count; i++)
            {
                var calcResult = ApplyOperator(accumulator, Double.Parse(listOfOperands[i + 1]), listOfOperators[i]);
                accumulator = calcResult.HasValue ? calcResult.Value : accumulator;
            }

            return accumulator;
        }

        private double? ApplyOperator(double operand1, double operand2, string operatorString)
        {
            switch (operatorString)
            {
                case "+":
                    return operand1 + operand2;
                case "-":
                    return operand1 - operand2;
                case "*":
                    return operand1 * operand2;
                case "/":
                    return operand1 / operand2;
                default:
                    return null;
            }
        }
    }
}
