﻿using System;

namespace StringEvaluation.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Calculator starting...");
            System.Console.WriteLine("Enter a valid query or press esc to exit:");

            var stringEvaluator = new StringEvaluator();

            while (System.Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
                var userInput = System.Console.ReadLine();
                var result = stringEvaluator.Evaluate(userInput);
                System.Console.WriteLine($"Result is: {result}");
                System.Console.WriteLine("Enter a valid query or press esc to exit:");
            }
                        
            System.Console.WriteLine("Application exiting...");
            System.Threading.Thread.Sleep(2000);
        }
    }
}
