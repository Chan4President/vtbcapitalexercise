﻿using NUnit.Framework;
using StringEvaluation.Console;

namespace StringEvaluation.Tests
{
    [TestFixture]
    public class StringEvaluatorFixture
    {
        private StringEvaluator _stringEvaluator;

        [SetUp]
        public void SetUp()
        {
            _stringEvaluator = new StringEvaluator();
        }

        [TestCase("(1 + 2)", 3)]
        [TestCase("(1 + 2) * (2 + 3) * (3 * (2 + 4))", 270)]
        public void Given_a_valid_input_when_passed_for_evaluation_then_result_computed(string input, double expectedResult)
        {
            var result = _stringEvaluator.Evaluate(input);

            Assert.AreEqual(result, expectedResult);
        }
    }
}
